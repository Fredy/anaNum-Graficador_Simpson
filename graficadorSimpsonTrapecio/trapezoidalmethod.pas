unit TrapezoidalMethod;

{$mode objfpc}{$H+}

interface

uses
   math, mCalculator, mFuncImp, mMatrix;

type
   TArrStr = array of string;

   TTrapezoidalMethod = class
   public
      _function : TCalculator;
      _low : real;
      _high : real;
      _n : integer;
   public
      constructor create();
      destructor destroy(); override;
      procedure setFunction(func : string);
      procedure setLowHigh(low, high : real);
      procedure setNumOfIntervals(n : integer);
      function eval() : real;
   end;

implementation

constructor TTrapezoidalMethod.create();
begin
   _function := TCalculator.create();
end;

destructor TTrapezoidalMethod.destroy();
begin
   _function.destroy();
end;

procedure TTrapezoidalMethod.setFunction(func : string);
begin
   _function.solveExpression(func);
end;

procedure TTrapezoidalMethod.setLowHigh(low, high : real);
begin
   _low := low;
   _high := high;
end;

procedure TTrapezoidalMethod.setNumOfIntervals(n : integer);
begin
   _n := n;
end;

function TTrapezoidalMethod.eval() : real;
var
   h, xi, sum, aux : real;
   i : integer;
begin
   h := (_high - _low) / _n;
   sum := 0;
   for i := 1 to _n - 1 do
   begin
      xi := _low + i * h;
      sum := sum + _function.solveSavedExpression(['x'], [xi])[0][0];
   end;
   aux := _function.solveSavedExpression(['x'], [_low])[0][0] +
      _function.solveSavedExpression(['x'], [_high])[0][0];
   aux := aux / 2 + sum;
   aux := aux * h;
   Result := aux;
end;

end.
