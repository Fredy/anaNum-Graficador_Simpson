unit graph;

{$mode objfpc}{$H+}

interface

uses
   Classes, SysUtils, FileUtil, TAGraph, TASeries, TAFuncSeries, Forms, Controls,
   Graphics, Dialogs, ExtCtrls, StdCtrls, ColorBox, TAChartUtils,
   TASources, TATools  ,TACustomSeries, mCalculator;

type

   { TfrmGraficadora }

   TfrmGraficadora = class(TForm)
      chrGrafica: TChart;
      Area: TAreaSeries;
      Funcion: TFuncSeries;
      Plotear: TLineSeries;
      EjeX: TConstantLine;
      EjeY: TConstantLine;
      procedure AreaGetMark(out AFormattedMark: String; AIndex: Integer);
      procedure FormCreate(Sender: TObject);
      procedure FormDestroy(Sender: TObject);
      procedure FormShow(Sender: TObject);
      procedure FuncionCalculate(const AX: Double; out AY: Double);

      Procedure GraficarFuncionConPloteo();

   private
      { private declarations }
      calculator : TCalculator;

      areaLow : real;
      areaHigh : real;
      funcLow : real;
      funcHigh : real;

      function f( x: Double ): Double;

   public
      { public declarations }
      procedure setData(low, high : real; funct : string);
   end;

var
   frmGraficadora: TfrmGraficadora;


implementation

{$R *.lfm}

{ TfrmGraficadora }

procedure TfrmGraficadora.setData(low, high: real; funct: string);
var
   tmp : real;
begin
   tmp := (high - low) / 2;
   funcLow := low - tmp * 4;
   funcHigh := high + tmp * 4;
   areaLow := low;
   areaHigh := high;
   calculator.solveExpression(funct);
end;

function TfrmGraficadora.f( x: Double ): Double;
begin
   Result := calculator.solveSavedExpression(['x'], [x])[0][0];
end;

procedure TfrmGraficadora.GraficarFuncionConPloteo;
var
   i: Integer;
   Max, Min, h, NewX, NewY: Real;
begin
   Funcion.Active:= False;
   Area.Active:= False;

   Max :=  funcHigh;
   Min :=  funcLow;

   h:= abs((Max - Min)/(100 * Max));

   NewX:= funcLow;
   //TODO SET COLOR!!

   while NewX < Max do begin
      NewY := f(NewX);
      Plotear.AddXY(NewX, NewY);
      if (NewX >= areaLow) and (NewX <= areaHigh) then
         Area.AddXY(NewX, NewY);
      NewX:= NewX + h;
   end;

   Plotear.Active:= true;
   Area.Active:= true;
end;

procedure TfrmGraficadora.AreaGetMark(out AFormattedMark: String;
                                      AIndex: Integer);
begin

end;

procedure TfrmGraficadora.FormCreate(Sender: TObject);
begin
   calculator := TCalculator.create();
   calculator.solveExpression('x = 0');
end;

procedure TfrmGraficadora.FormDestroy(Sender: TObject);
begin
   calculator.destroy();
end;

procedure TfrmGraficadora.FormShow(Sender: TObject);
begin

   Area.Clear;
   Plotear.Clear;
   GraficarFuncionConPloteo()
end;

procedure TfrmGraficadora.FuncionCalculate(const AX: Double; out AY: Double);

begin
   AY := f( AX ) ;

end;

end.
