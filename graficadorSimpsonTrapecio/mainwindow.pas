unit mainwindow;

{$mode objfpc}{$H+}

interface

uses
   Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
   TrapezoidalMethod, Simpson1_3Method, Simpson3_8Method, graph;

type

   { TForm1 }

   TForm1 = class(TForm)
      btnEval: TButton;
      btnGraph: TButton;
      cbxMethods: TComboBox;
      edtHigh: TEdit;
      edtLow: TEdit;
      edtNumOfIntervals: TEdit;
      edtFunction: TEdit;
      lblHigh: TLabel;
      lblLow: TLabel;
      lblNumOfIntervals: TLabel;
      lblFunction: TLabel;
      procedure btnEvalClick(Sender: TObject);
      procedure btnGraphClick(Sender: TObject);
      procedure FormCreate(Sender: TObject);
      procedure FormDestroy(Sender: TObject);
   private
      trapezoidal : TTrapezoidalMethod;
      simpson1_3 : TSimpson1_3Method;
      simpson3_8 : TSimpson3_8Method;
      //mygraph : TfrmGraficadora;
   public
      { public declarations }
   end;

var
   Form1: TForm1;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.btnEvalClick(Sender: TObject);
var
   high, low : real;
   intervals : integer;
begin
   if (edtFunction.GetTextLen = 0) then
   begin
      ShowMessage('Ingrese una función.');
      exit;
   end;
   if (edtLow.GetTextLen = 0) then
   begin
      ShowMessage('Ingrese el límite inferior.');
      exit;
   end;
   if (edtHigh.GetTextLen = 0) then
   begin
      ShowMessage('Ingrese el límite superior.');
      exit;
   end;
   if (edtNumOfIntervals.GetTextLen = 0) then
   begin
      ShowMessage('Ingrese la cantidad de intervalos.');
      exit;
   end;

   high := strToFloat(edtHigh.text);
   low := strToFloat(edtLow.text);
   intervals := StrToInt(edtNumOfIntervals.text);
   frmGraficadora.setData(low, high,edtFunction.text);
   if (cbxMethods.ItemIndex = 0) then
   begin
      trapezoidal.setFunction(edtFunction.text);
      trapezoidal.setLowHigh(low, high);
      trapezoidal.setNumOfIntervals(intervals);

      ShowMessage(floatToStr(trapezoidal.eval()));
   end
   else if (cbxMethods.ItemIndex = 1) then
   begin
      if (intervals mod 2) <> 0 then
      begin
         ShowMessage('El intervalo debe ser múltiplo de 2');
         exit;
      end;
      simpson1_3.setFunction(edtFunction.text);
      simpson1_3.setLowHigh(low, high);
      simpson1_3.setNumOfIntervals(intervals);

      ShowMessage(floatToStr(simpson1_3.eval()));
   end
   else
   begin
      if (intervals mod 3) <> 0 then
      begin
         ShowMessage('El intervalo debe ser múltiplo de 3');
         exit;
      end;
      simpson3_8.setFunction(edtFunction.text);
      simpson3_8.setLowHigh(low, high);
      simpson3_8.setNumOfIntervals(intervals);

      ShowMessage(floatToStr(simpson3_8.eval()));
   end


end;

procedure TForm1.btnGraphClick(Sender: TObject);
begin
    frmGraficadora.Show;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
   trapezoidal := TTrapezoidalMethod.create();
   simpson1_3 := TSimpson1_3Method.create();
   simpson3_8 := TSimpson3_8Method.create();
   //mygraph := TfrmGraficadora.FormCreate();

   cbxMethods.Items.Add('Trapecio');
   cbxMethods.Items.Add('Simpson 1/3');
   cbxMethods.Items.Add('Simpson 3/8');
   cbxMethods.ItemIndex := 0;
end;

procedure TForm1.FormDestroy(Sender: TObject);
begin
   trapezoidal.destroy();
   simpson1_3.destroy();
   simpson3_8.destroy();
end;

end.
