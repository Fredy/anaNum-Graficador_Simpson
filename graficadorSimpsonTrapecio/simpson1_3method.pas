unit Simpson1_3Method;

{$mode objfpc}{$H+}

interface

uses
   math, mCalculator, mFuncImp, mMatrix;

type
   TArrStr = array of string;

   TSimpson1_3Method = class
   public
      _function : TCalculator;
      _low : real;
      _high : real;
      _n : integer;
   public
      constructor create();
      destructor destroy(); override;
      procedure setFunction(func : string);
      procedure setLowHigh(low, high : real);
      procedure setNumOfIntervals(n : integer);
      function eval() : real;
   end;

implementation

constructor TSimpson1_3Method.create();
begin
   _function := TCalculator.create();
end;

destructor TSimpson1_3Method.destroy();
begin
   _function.destroy();
end;

procedure TSimpson1_3Method.setFunction(func : string);
begin
   _function.solveExpression(func);
end;

procedure TSimpson1_3Method.setLowHigh(low, high : real);
begin
   _low := low;
   _high := high;
end;

procedure TSimpson1_3Method.setNumOfIntervals(n : integer);
begin
   _n := n;
end;

function TSimpson1_3Method.eval() : real;
var
   h, xi, sumOdds, sumEvens, aux : real;
   i : integer;
begin
   h := (_high - _low) / _n;
   sumEvens := 0;
   sumOdds := 0;
   for i := 1 to floor(_n/2) - 1 do
   begin
      xi := _low + 2 * i * h;
      sumEvens := sumEvens + _function.solveSavedExpression(['x'], [xi])[0][0];
   end;

   for i := 1 to floor(_n/2) do
   begin
      xi := _low + (2 * i - 1) * h;
      sumOdds := sumOdds + _function.solveSavedExpression(['x'], [xi])[0][0];
   end;

   aux := _function.solveSavedExpression(['x'], [_low])[0][0] +
      _function.solveSavedExpression(['x'], [_high])[0][0];
   aux := aux + 2 * sumEvens + 4 * sumOdds;
   aux := aux * h / 3;
   Result := aux;
end;

end.
